import matplotlib.pyplot as plt
import json
### Data Set ###
dataset = 'poker-lsn.csv'
# dataset = 'covtypeNorm.csv'

f = open(dataset+'_sample_x.json','r')
sample_x = json.loads(f.read())
f.close()

f = open(dataset+'_err_VFDT.json','r')
acc_VFDT = json.loads(f.read())
f.close()

f = open(dataset+'_time_VFDT.json','r')
runtime_VFDT = json.loads(f.read())
f.close()

f = open(dataset+'_err_OSM.json','r')
acc_OSM = json.loads(f.read())
f.close()

f = open(dataset+'_time_OSM.json','r')
runtime_OSM = json.loads(f.read())
f.close()

### Error Rate Plot ###
l1, = plt.plot(sample_x,acc_VFDT,color='#0000FF',label='VFDT')
l2, = plt.plot(sample_x,acc_OSM,color='red',label='OSM')
plt.ylabel('Error rate')

### Run Time Plot ###
# l1, = plt.plot(sample_x,runtime_VFDT,color='red',label='VFDT')
# l2, = plt.plot(sample_x,runtime_OSM,color='green',label='OSM')
# plt.ylabel('Total run-time')

plt.title(dataset)
plt.xlabel('#Sample')
plt.legend()
plt.show()
