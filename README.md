# EVFDT

Course: Data Mining(NTUST)

Paper: Enhancing VFDT with Local Split-Time Predictions


## Installation:
- Install Python3.7
- Install Graphviz

## Setup:

### 1. Python3.7:

    pip3 install pandas
    pip3 install -U scikit-learn
    pip3 install openpyxl

### 2. Graphviz:
- Set environment variables path: "…\Graphviz2.38\bin"

## How to run:

### 1. EVFDT
- Run "evfdt.py".
    - PS: It will create the .json and .xlsx files.
- Put .json file into result folder.
- Modify and run "Toplot.py" to create the png file.

### 2. Tree visualization
- Run "evfdt_visualize.py"
    - PS: It will create the .dot file and draw the tree.